# rss2mastodon

Update your [Mastodon](https://github.com/tootsuite/mastodon) with accounts with RSS feeds.

![Join Mastodon](https://files.mastodon.social/site_uploads/files/000/000/001/original/DN5wMUeVQAENPwp.jpg_large.jpeg)
## Overview

1. Uses [PHP-CLI](http://php.net/manual/en/features.commandline.usage.php) and [Composer](https://getcomposer.org). 
2. No database or server required
3. Fast, secure, reliable and easy to maintain
4. Fully extendable
5. supports copying information from: twitter, rss 
6. supports posting into a mastodon account

## How-to

1. Install docker & docker-compose (you can also run directly without docker)
2. Copy the config_example.yml to config.yml
3. Edit config.yml to your own needs
4. Run with the command to sync rss with mastodon:

 ```
  $ docker-compose up
 ```

## Setup a cronjob to run every 10 minutes
```
  $ crontab -e
```
add the line 
```
*/10 * * * * cd mypath && docker-compose up
```


## Upgrading
1. ``` $ git pull```
2. ``` $ docker-compose build```

## Q&A

##### Q: Where do I get my access token for mastodon??
##### A: Get your token here: [token generator](https://takahashim.github.io/mastodon-access-token/?code=1ab312089d27d2bd6cb9078484220bbb9baf4c5aeec25d25b6fe159c9e3aad02)

##### Q: Where do I get my access token for twitter??
##### A: Create an app in [here](https://apps.twitter.com/)


## Roadmap

 - Support posting to twitter
 - Support posting to facebook pages