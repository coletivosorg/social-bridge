<?php 
namespace Rss2Mastodon\Adapters;

use \Curl\Curl;
use \Rss2Mastodon\Entities\Article;
use \Longman\TelegramBot\Telegram;
use \Longman\TelegramBot\Request;
use \Longman\TelegramBot\Exception\TelegramException;
use \Longman\TelegramBot\Exception\TelegramLogException;

class TelegramAdapter
{
    /** @var \Curl\Curl **/
    private $curl;

    private $telegramBotToken;
    
    public function __construct()
    {
    }
    
    public function connect($apiKey)
    {
        try {
            // Create Telegram API object
            $telegram = new Telegram($apiKey);
            
        } catch (TelegramException $e) {
            echo "Exception " . $e;
            // Longman\TelegramBot\TelegramLog::error($e);
        } catch (TelegramLogException $e) {
            // Uncomment this to output log initialisation errors (ONLY FOR DEVELOPMENT!)
            // echo $e;
        }
    }

    private function sendMessage($message, $roomId)
    {
        
        echo "Sending '$message' to '$roomId'";
        try{
            $result = Request::sendMessage([
                'chat_id' => $roomId,
                'text'    => $message,
                'parse_mode' => 'markdown'
            ]);
        } catch(TelegramException $e) {
            echo "Exception " . $e; 
        } catch (TelegramLogException $e) {
            // Uncomment this to output log initialisation errors (ONLY FOR DEVELOPMENT!)
            // echo $e;
        }
        
    }

    private function sendData($data, $roomId) {
        $data['chat_id'] = $roomId;
        echo "Sending data to '$roomId'";
        try{
            $result = Request::sendPhoto($data);
        } catch(TelegramException $e) {
            echo "Exception " . $e; 
        } catch (TelegramLogException $e) {
            // Uncomment this to output log initialisation errors (ONLY FOR DEVELOPMENT!)
            // echo $e;
        }
    }

    private function sendPicture($imageUrl, $roomId) {
        echo "Sending '$imageUrl' to '$roomId'";
        try{
            $result = Request::sendPhoto([
                'chat_id' => $roomId,
                'photo'   => $imageUrl,
                'parse_mode' => 'Html',
            ]);
        } catch(TelegramException $e) {
            echo "Exception " . $e; 
        } catch (TelegramLogException $e) {
            // Uncomment this to output log initialisation errors (ONLY FOR DEVELOPMENT!)
            // echo $e;
        }
    }

    public function addArticles($articles, $chatId) {
        foreach($articles as $article) {
            $message = str_replace("</p>","\n\n</p>",$article->html);

            $message = strip_tags($message);
            $message .= "Fonte: $article->link";
            if (!empty($article->image)) 
            {
                $data['photo'] = $article->image;
                $data['caption'] = $message;
                
                $this->sendData($data, $chatId);
            }
        }
    }
/*
    public function listen($bot_api_key,$bot_username,$hook_url) {
        try {
            // Create Telegram API object
            $telegram = new Longman\TelegramBot\Telegram($bot_api_key, $bot_username);
        
            // Set webhook
            $telegram->useGetUpdatesWithoutDatabase();
            $telegram->handleGetUpdates();
            $result = $telegram->setWebhook($hook_url);
            if ($result->isOk()) {
                echo $result->getDescription();
            }
        } catch (Longman\TelegramBot\Exception\TelegramException $e) {
            // log telegram errors
            // echo $e->getMessage();
        }
    }



    public function readArticles($articles, $chatId) {
        foreach($articles as $article) {
            $message = str_replace("</p>","\n\n</p>",$article->html);

            $message = strip_tags($message);
            $message .= "Fonte: $article->link";
            if (!empty($article->image)) 
            {
                $data['photo'] = $article->image;
                $data['caption'] = $message;
                
                $this->sendData($data, $chatId);
            }
        }
    }
    */
}



