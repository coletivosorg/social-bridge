<?php 
namespace Rss2Mastodon\Adapters;

use \Curl\Curl;
use \Rss2Mastodon\Entities\Article;

class MastodonAdapter
{
    /** @var \Curl\Curl **/
    private $curl;
    
    private $mastodonAccount;
    
    public function __construct(Curl $curl)
    {
        $this->curl = $curl;
    }
    
    public function setSettings($mastodonAccount)
    {
        $this->mastodonAccount = $mastodonAccount;
    }
    
    public function addArticles($articles) 
    {
        foreach($articles as $article) {
            // if(!$this->addArticle($article))
            // {
                // return false;
            // }
        }
        return true;
    }
    
    public function addArticle(Article $article)
    {
        $content = \utf8_encode($article->title);
        $link = $article->link;
        
        return $this->postToMastodon($content);
    }
    
    public function postToMastodon($content)
    {
        echo "  Posting to mastodon: " . $content . chr(10);
        $action = '/api/v1/statuses';
        $url = $this->mastodonAccount['url'] . $action . '?access_token=' . $this->mastodonAccount['token'];
        
        // $get = $this->curl->post($url, array('status' => $content));
        
        if ($this->curl->httpStatusCode != "200")
        {
            echo '  Error, posting to mastodon ' . $this->mastodonAccount['url'] . chr(10);
            return false;
        }
        return true;
    }
}

