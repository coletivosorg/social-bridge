<?php 
namespace Rss2Mastodon\Adapters;
use \SimplePie;
use \League\HTMLToMarkdown\HtmlConverter;

use Rss2Mastodon\Entities\Article;

class RssAdapter{
    private $feedService;
    private $htmlConverterService;

    public function __construct(SimplePie $feedService)
    {
        $this->feedService = $feedService;
        $this->htmlConverterService = new HtmlConverter(array('strip_tags' => true));
    }
    
    public function getFeed($feedUrl)
    {
        $articles = array();
        echo  '   Rss fetching..... ' . $feedUrl . chr(10);
        
        $rss = \Feed::loadRss($feedUrl);
        
        foreach ($rss->item as $item)
        {
            $article = new Article();
            $itemArr = json_decode(json_encode($item), true);
            
            $article->title = $itemArr['title'];
            // $article->title = utf8_decode($itemArr['title']);

            $article->postDate = date("Y-m-d H:i",strtotime($itemArr['pubDate'] ));
            $article->link = $itemArr['guid'];
            
            if (isset($itemArr['enclosure'])) {
                if (isset($itemArr['enclosure']['@attributes'])) {
                    if (in_array($itemArr['enclosure']['@attributes']['type'],array('image/jpeg', 'image/png', 'image/jpg'))) {
                        $article->image = $itemArr['enclosure']['@attributes']['url'];
                    }
                }
            }
            
            if (!empty($itemArr['description'])) {
                // echo '    ' .  $itemArr['link'] . ' article description' . chr(10);
                $article->markdown = $this->htmlConverterService->convert($itemArr['description']);

                $article->markdown = str_replace('<','',$article->markdown);
                $article->markdown = str_replace('>','',$article->markdown);
                $article->markdown = str_replace('\\','',$article->markdown);
                $article->html = $itemArr['description'];
            }

            $articles[] = $article;
        }
        return $articles;
    }
}

