<?php 
namespace Rss2Mastodon\Adapters;

use Rss2Mastodon\Entities\Article;

require_once(__DIR__ . '/../../libs/TwitterAPIExchange.php');

class TwitterAdapter
{
	private $settings;
    
    public function setSettings($settings)
    {
        $this->settings = $settings;
    }
	function getSearch($query)
	{
		
		return $this->queryGET('https://api.twitter.com/1.1/search/tweets.json',"?q=".$query."&count=100");
	}
	
	function getTimeline($twitteraccount)
	{
        echo  '   Twitter fetching timeline..... ' . $twitteraccount . chr(10);
        
		$twits =  $this->queryGET('https://api.twitter.com/1.1/statuses/user_timeline.json',"?screen_name=".$twitteraccount."&count=100");
        
        foreach($twits as $twit)
		{
            
			if(empty($twit['text']))
			{
				continue;
			}
            
			// if(isset($twit['retweeted_status']) && is_array($twit['retweeted_status'])){
				// continue;
			// }
            
            $article = new Article();
            $article->title = utf8_decode($twit['text']);
            $article->postDate = date("Y-m-d H:i",strtotime($twit['created_at'] ));
            $article->link = "https://twitter.com/".$twit['user']['screen_name'].'/status/'.$twit['id_str'];
            
            $articles[] = $article;
		}
        return $articles;
	}
	
	function queryGET($url,$query)
	{
		
		$twitter = new \TwitterAPIExchange($this->settings);
		
		$requestMethod = 'GET';
		try{
			$json = $twitter->setGetfield($query)->buildOauth($url, $requestMethod)->performRequest();
		}catch(Exception $e)
		{
			
		}
	
		$data = json_decode($json,1);
		return $data;
	}
}