<?php
namespace Rss2Mastodon\Entities;

class Article{
    public $title;
    public $postDate;
    public $link;
    public $html;
    public $markdown;
    public $image;
}