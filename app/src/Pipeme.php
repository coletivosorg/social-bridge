<?php 

namespace Rss2Mastodon;

use Rss2Mastodon\Adapters\RssAdapter;
use Rss2Mastodon\Adapters\TwitterAdapter;
use Rss2Mastodon\Adapters\MastodonAdapter;
use Rss2Mastodon\Adapters\TelegramAdapter;

class Pipeme{
    private $config;
    
    /** @var ConfigService **/
    private $configService;
    
    /** @var Adapters\RssAdapter **/
    private $rssAdapter;
    
    /** @var Adapters\TwitterAdapter **/
    private $twitterAdapter;
    
    /** @var Adapters\MastodonAdapter **/
    private $mastodonAdapter;

    /** @var Adapters\TelegramAdapter **/
    private $telegramAdapter;
    
    public function __construct(ConfigService $configService, RssAdapter $rssAdapter, TwitterAdapter $twitterAdapter, MastodonAdapter $mastodonAdapter, TelegramAdapter $telegramAdapter)
    {
        $this->configService = $configService;
        $this->rssAdapter = $rssAdapter;
        $this->twitterAdapter = $twitterAdapter;
        $this->mastodonAdapter = $mastodonAdapter;
        $this->telegramAdapter = $telegramAdapter;
    }
    
    public function run($configPath)
    {
        $this->config = $this->configService->loadConfig($configPath);
        if (isset($this->config['twitter'])) {
            $this->twitterAdapter->setSettings($this->config['twitter']);
        }
        
        
        $lastUpdate = $this->config['last_update'];
        
        foreach($this->config['accounts'] as $accountName => $account) {
            
            if (isset($account['disabled'])) {
                continue;
            }
            echo 'Running account: ' . $account['name'] . chr(10);

            if (!isset($account['source']) || !isset($account['target'])) {
                echo "No sources/target found..." . chr(10);
                continue;
            }
            
            try {
                $articles = $this->fetchArticles($account['source'], $lastUpdate, $this->config['limit']);
                if ($account['target']['type'] == 'telegram')
                {
                    $this->telegramAdapter->connect($account['target']['botToken']);
                    $this->telegramAdapter->readArticles($articles, $account['target']['chatId']);
                }
                $this->postArticles($account['target'], $articles);
            }catch(\Exception $e) {
                echo "faillled... " . $e->getMessage() . chr(10);
            }
        }
        
        $this->config['last_update'] = date('Y-m-d H:i');
        $this->configService->saveConfig($configPath, $this->config);
        
    }

    public function runHooks($configPath)
    {
        $this->config = $this->configService->loadConfig($configPath);

        foreach($this->config['accounts'] as $accountName => $account) {
            
            if (isset($account['disabled'])) {
                continue;
            }
            echo 'Running account: ' . $account['name'] . chr(10);

            if (!isset($account['source']) || !isset($account['target'])) {
                echo "No sources/target found..." . chr(10);
                continue;
            }
            
            try {
                $articles = $this->fetchArticles($account['source'], $lastUpdate, $this->config['limit']);

                if ($account['target']['type'] == 'telegram')
                {
                    $this->telegramAdapter->connect($account['target']['botToken']);
                    $this->telegramAdapter->readArticles($articles, $source['chatId']);
                }
                
                $this->postArticles($account['target'], $articles);
            }catch(\Exception $e) {
                echo "faillled... " . $e->getMessage() . chr(10);
            }
        }
        
        $this->config['last_update'] = date('Y-m-d H:i');
        $this->configService->saveConfig($configPath, $this->config);
        
    }
    
    public function fetchArticles($source, $lastUpdate, $limit = 5)
    {
        $articles = array();
        
            switch($source['type'])
            {
                case 'rss':
                    $feedUrl = $source['feed'];
                    $feedArticles = $this->rssAdapter->getFeed($feedUrl);
                    $newArticles = $this->filterBy($feedArticles, $lastUpdate);
                    $articles = array_merge($articles, $newArticles);
                break;
                case 'twitter':
                    $twitterArticles = $this->twitterAdapter->getTimeline($twitterUser);
                    $newArticles = $this->filterBy($twitterArticles, $lastUpdate);
                    $articles = array_merge($articles, $newArticles);
                break;
                case 'telegram':
                    echo '  Telegram updates skipped, need to run it as a hook' . chr(10);
                break;
                // case 'facebook':
                    // foreach($sourceConfig as $facebookUser )
                    // {
                        // $this->facebookAdapter->getWall
                    // }
                default: 
                echo '   not supported yet... ' . $source['type']  . chr(10);
            }
        
        if (empty($articles)) { 
            return array();
        }

        usort($articles, function(Entities\Article $a1, Entities\Article $a2) {
            $v1 = strtotime($a1->postDate);
            $v2 = strtotime($a2->postDate);
            return !($v1 - $v2);
        });
        
        $articles = \array_slice($articles, 0, $limit);

        usort($articles, function(Entities\Article $a1, Entities\Article $a2) {
            $v1 = strtotime($a1->postDate);
            $v2 = strtotime($a2->postDate);
            return $v1 - $v2;
        });
        
        return $articles;
    }
    
    public function postArticles($target, $articles)
    {
        if(empty($articles)) {
            echo '    No articles to post' . chr(10);
            return;
        }

        echo '  Posting articles to ' . $target['type'] . chr(10);
        
        switch($target['type'])
        {
            case 'mastodon':
                $this->mastodonAdapter->setSettings($target);
                $this->mastodonAdapter->addArticles($articles);
            break;
            case 'telegram':
                $this->telegramAdapter->addArticles($articles, $target['chatId']);
                break;
            default: 
            echo '   not supported yet...' . chr(10);
        }
    
    }
    
    public function filterBy($articles, $lastUpdate = 'now')
    {
        $newLastUpdate = $lastUpdate;
        $newArticles = array();
        $i = 0;
        foreach($articles as $article) 
        {
            if (strtotime($lastUpdate) >= strtotime($article->postDate) && $lastUpdate != 0)
            {
                continue;
            }
            $i++;
            if ($newLastUpdate == 0) {
                $newLastUpdate = $article->postDate;
            }
            if (strtotime($newLastUpdate) < strtotime($article->postDate))
            {
                $newLastUpdate = $article->postDate;
            }
            $newArticles[] = $article;
            if ($i > 10) {
                break;
            }
        }
        return $newArticles;
    }
}

